//
//  EffectiveSwiftFlowSpec.swift
//  es-RomanUITests
//
//  Created by Roman on 9/29/19.
//  Copyright © 2019 xyz.boarlabs. All rights reserved.
//

import Foundation
import Quick
import Nimble
import XCTest
class EffectiveSwiftFlowSpec: QuickSpec {
    let firstName = "Roman"
    let lastName = "Hryshchyshyn"

    override func spec() {
        describe("Es application") {
            it("Should register") {
                let app = XCUIApplication()
                app.launch()
                app.textFields["FirstNameTextField"].tap()
                app.textFields["FirstNameTextField"].typeText(self.firstName)
                app.textFields["LastNameTextField"].tap()
                app.textFields["LastNameTextField"].typeText(self.lastName)

                app.buttons["Return"].tap()

                expect(app.textFields["LastNameTextField"].text) == self.lastName
            }
        }
    }
}
