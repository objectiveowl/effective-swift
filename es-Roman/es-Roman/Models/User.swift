//
//  User.swift
//  es-Roman
//
//  Created by Roman on 9/29/19.
//  Copyright © 2019 xyz.boarlabs. All rights reserved.
//

import Foundation

struct User {
    let firstName: String
    let lastName: String
    let email: String
    let password: String
}
