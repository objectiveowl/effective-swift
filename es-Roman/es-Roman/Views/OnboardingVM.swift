//
//  OnboardingVM.swift
//  es-Roman
//
//  Created by Roman on 9/29/19.
//  Copyright © 2019 xyz.boarlabs. All rights reserved.
//

import Foundation
import ReaktiveBoar
import Swinject
import SwinjectAutoregistration
import ReactiveKit
import Bond

class OnboardingVM: VCVM {
    let firstName = Property("")
    let lastName = Property("")
}
