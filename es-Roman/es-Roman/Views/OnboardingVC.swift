//
//  ViewController.swift
//  es-Roman
//
//  Created by Roman on 9/28/19.
//  Copyright © 2019 xyz.boarlabs. All rights reserved.
//

import UIKit
import ReaktiveBoar
import ReactiveKit

class OnboardingVC: UIViewController {
    @IBOutlet public var firstNameTextField: UITextField!
    @IBOutlet public var lastNameTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension OnboardingVC: VCView {
    func advise(vm: OnboardingVM, bag: DisposeBag) {
        vm.firstName.bidirectionalMap(to: { $0 }, from: { $0 ?? "" })
            .bidirectionalBind(to: firstNameTextField.reactive.text)
            .dispose(in: bag)

        vm.firstName.observe(with: {objec in
            print(objec.element!)
        }).dispose(in: bag)

        vm.lastName.bidirectionalMap(to: {$0}, from: {$0 ?? "" }).bidirectionalBind(to: lastNameTextField.reactive.text)
            .dispose(in: bag)
    }

    typealias VMType = OnboardingVM
}
