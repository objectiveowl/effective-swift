//  OnboardingVCSpec.swift
//  OnboardingVCSpec
//
//  Created by Roman on 9/28/19.
//  Copyright © 2019 xyz.boarlabs. All rights reserved.
//

import XCTest
import Quick
import Nimble

@testable import es_Roman

class OnboardingVCSpec: QuickSpec {
    override func spec() {
        describe("Obvoarding View Controller") {
            it("Shoulbe be created") {
                let vc = UIStoryboard(
                    name: OnboardingVC.className, bundle: Bundle(for: OnboardingVC.self)
                    // swiftlint:disable:next force_cast
                ).instantiateInitialViewController() as! OnboardingVC

                vc.awakeFromNib()
                vc.beginAppearanceTransition(true, animated: false)
                vc.endAppearanceTransition()
                expect(vc.firstNameTextField).notTo(beNil())
            }
        }
    }
}
